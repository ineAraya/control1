package cl.duoc.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnEntrar, btnRegistro;
    private EditText etUsuario, etClave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etUsuario= (EditText)findViewById(R.id.etUsuario);
        etClave= (EditText)findViewById(R.id.etClave);
        btnEntrar= (Button)findViewById(R.id.btnEntrar);
        btnRegistro =(Button)findViewById(R.id.btnRegistro);

        btnEntrar.setOnClickListener(this);
        btnRegistro.setOnClickListener(this);
     /*       @Override
            public void onClick(View v) {
                if(etClave.getText().toString().equals("admin") &&
                        etUsuario.getText().toString().equals("admin")){
                    Toast.makeText(LoginActivity.this, "Usuario Valido", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(LoginActivity.this, "Usuario no Valido", Toast.LENGTH_SHORT).show();

                }
            }
        });*/

    }


    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnEntrar){
          if(etUsuario.getText().toString().equals("admin") &&
                    etUsuario.getText().toString().equals("admin")){
              Toast.makeText(this, "Usuario Valido", Toast.LENGTH_SHORT).show();

            }else{
              Toast.makeText(this, "Usuario no valido", Toast.LENGTH_SHORT).show();
          }}else if (v.getId()==R.id.btnRegistro){
            Intent i = new Intent(LoginActivity.this, RegistroActivity.class);
           startActivity(i);

        }


        }
    }


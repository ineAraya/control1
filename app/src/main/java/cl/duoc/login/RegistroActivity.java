package cl.duoc.login;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

import cl.duoc.login.bd.RegistrarPersona;
import cl.duoc.login.entidades.Persona;

public class RegistroActivity extends AppCompatActivity {

    private Button btnRegistrar, etFecha;
    private Button btnLimpiar;
    private EditText etUsuario, etNombres, etApellido, etRut, etClave1, etClave2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        etUsuario= (EditText)findViewById(R.id.etUsuario);
        etNombres= (EditText)findViewById(R.id.etNombres);
        etApellido= (EditText)findViewById(R.id.etApellido);
        etRut= (EditText)findViewById(R.id.etRut);
        etFecha= (Button)findViewById(R.id.etFecha);
        etClave1= (EditText)findViewById(R.id.etClave1);
        etClave2= (EditText)findViewById(R.id.etClave2);

        btnRegistrar=(Button)findViewById(R.id.btnRegistrar);
        btnLimpiar=(Button)findViewById(R.id.btnLimpiar);

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resgistrarPersona();

            }
        });

        etFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().show();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiarTodo();
            }
        });
    }

    public void resgistrarPersona(){
        String mensajeError="";

        if(etUsuario.getText().toString().length() < 1){
            mensajeError += "Ingrese Usuario \n";
        }
        if(etNombres.getText().toString().length()<1){
            mensajeError+="Ingrese Nombre \n";
        }
        if(etApellido.getText().toString().length()<1){
            mensajeError+="Ingrese Apellido \n";
        }
       /* if(!isValidarRut(etRut.getText().toString())){
            mensajeError+="Ingrese un rut valido \n";
        }*/
       if(etRut.getText().toString().length()<1){
           mensajeError+="Ingrese un rut valido \n";
       }
        if(etClave1.getText().toString().length()<1 ){
            mensajeError+="contraseña no valida \n";

        }
        if(etClave2.getText().toString().length()!= etClave1.getText().toString().length()){
            mensajeError+="contraseña no valida \n";

        }
        if(mensajeError.length()>0){
            Toast.makeText(RegistroActivity.this, mensajeError, Toast.LENGTH_SHORT).show();
        }
        else{
            Persona regis= new Persona();
            regis.setUsuario(etUsuario.getText().toString());
            regis.setNombre(etNombres.getText().toString());
            regis.setFecha(etFecha.getText().toString());
            regis.setClave(etClave1.getText().toString());
            regis.setRepetirClave(etClave2.getText().toString());
            regis.setRut(etRut.getText().toString());

            RegistrarPersona.agregarRegistro(regis);
            Toast.makeText(this, "Registro guardado correctamente", Toast.LENGTH_LONG).show();

        }

    }
    private boolean isValidarRut(String rut) {
        boolean validacion = false;
        try {
            rut =  rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            char dv = rut.charAt(rut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }
        return validacion;
    }

    private DatePickerDialog getDialog() {
        Calendar c = Calendar.getInstance();
        int anio = c.get(Calendar.YEAR);
        int mes = c.get(Calendar.MONTH);
        int dia = c.get(Calendar.DAY_OF_MONTH);


        return new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                etFecha.setText(year + "/" + formatDate((month + 1)) + "/" + formatDate(dayOfMonth));
            }
        }, anio, mes, dia);

    }

    private String formatDate(int value) {

        return (value > 9 ? value + "" : "0" + value);

    }

    private void limpiarTodo(){
        etUsuario.setText("");
        etNombres.setText("");
        etApellido.setText("");
        etRut.setText("");
        etFecha.setText("");
        etClave1.setText("");
        etClave2.setText("");
    }



}

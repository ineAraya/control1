package cl.duoc.login.bd;

import java.util.ArrayList;

import cl.duoc.login.entidades.Persona;

/**
 * Created by DUOC on 25-03-2017.
 */

public class RegistrarPersona {


    private static ArrayList<Persona> registros = new ArrayList<>();

    public static void agregarRegistro(Persona formulario){
        registros.add(formulario);
    }

    public static ArrayList<Persona> getFormularios(){
        return registros;
    }

}

package cl.duoc.login.entidades;

/**
 * Created by DUOC on 25-03-2017.
 */

public class Usuario {
    public String usuario;
    public String password;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
